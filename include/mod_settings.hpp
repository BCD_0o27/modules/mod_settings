#ifndef BCD_MODULE_SETTINGS_HPP
#define BCD_MODULE_SETTINGS_HPP

#pragma once
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include <stdlib.h>
#include <string>
#include "st7735_bcd.hpp"
#include "gfx.hpp"
#include "ch405labs_gfx_menu.hpp"
#include "../resources/fonts/Bm437_Acer_VGA_8x8.h"

#include "ch405labs_esp_wifi.hpp"
#include "ch405labs_esp_controller.hpp"
#include "ch405labs_esp_led.hpp"
#include "ch405labs_esp_debug.h"

#define TAG_MOD_SETTINGS   "SETTINGS"

using namespace espidf;
using namespace gfx;
using namespace gfxmenu;


/**
 * @brief Connect to wifi network
 * 
 * @param param 
 * @return int 
 */
template<typename Destination>
int connectWifi(void *param) {
    espwifi::wifiController Wifi;
    Destination *lcd = (Destination *)(((void **)param)[0]);
    espwifi::wifiStaConfig *cfg = (espwifi::wifiStaConfig *)(((void **)param)[1]);
    const font &font = Bm437_Acer_VGA_8x8_FON;

    TickType_t read_timout(pdMS_TO_TICKS(3000));

    char ssid[33];                                                              // Add +1 for null termination of ssid
    cfg->getSsid(ssid, sizeof(ssid));

    // Messages and their sizes
    //                                            "12345678901234567890"
    std::string connection_string               = "Connecting to ";
    std::string ssid_string(ssid);
    std::string connected_string                = "Disconnect...";
    std::string connect_string                  = "Connecting...";
    std::string ok_string                       = "OK";
    std::string fail_string                     = "FAIL";
    std::string timeout_string                  = "TOUT";

    ssize16 connection_msg_size = font.measure_text((ssize16)lcd->dimensions(), 
        (connection_string + ssid_string).c_str());
    ssize16 connected_string_size = font.measure_text((ssize16)lcd->dimensions(),
        connected_string.c_str());

   
    
    rect16 lcd_bounds = lcd->bounds();
    lcd->clear(lcd_bounds);

    rect16 msg_area(0, connection_msg_size.height, 
        lcd_bounds.x2 - ((fail_string.length()+1) * font.average_width()), 
        lcd_bounds.y2);

    rect16 status_area(msg_area.x2+font.average_width(), msg_area.y1, 
        lcd_bounds.x2, msg_area.y2);
    
    
    // Display connection attempt
    draw::text(*lcd,
                lcd_bounds,
                (connection_string + ssid_string).c_str(),
                font,
                color<typename Destination::pixel_type>::alice_blue);   

    // Connect
    // If we are connected, disconnect
    espwifi::wifiController::state_e wifi_state = Wifi.GetState();
    if(wifi_state == espwifi::wifiController::state_e::CONNECTED ||
        wifi_state == espwifi::wifiController::state_e::CONNECTING ||
        wifi_state == espwifi::wifiController::state_e::WAITING_FOR_IP) {
        
        draw::text(*lcd,
            msg_area,
            connected_string.c_str(),
            font,
            color<typename Destination::pixel_type>::red);

        wifi_err_t status = Wifi.disconnect();
        if(status != ESP_OK) {
            ESP_LOGE(TAG_WIFI, "Could not disconnect from AP.");
            draw::text(*lcd,
                status_area,
                fail_string.c_str(),
                font,
                color<typename Destination::pixel_type>::red);
            vTaskDelay(read_timout);                                            // Allow user to see status message
            return -1; // TODO proper error code
        }

        // Wait at most 10 seconds for disconnect
        for(int i = 0; i < 20; i++) {
            wifi_state = Wifi.GetState();
            if(wifi_state == espwifi::wifiController::state_e::DISCONNECTED || 
                wifi_state == espwifi::wifiController::state_e::READY_TO_CONNECT) {       
                break;
            } else {
                vTaskDelay(pdMS_TO_TICKS(500));
            }
        }
        if(wifi_state == espwifi::wifiController::state_e::DISCONNECTED || 
            wifi_state == espwifi::wifiController::state_e::READY_TO_CONNECT) {  
            draw::text(*lcd,
                status_area,
                ok_string.c_str(),
                font,
                color<typename Destination::pixel_type>::green);
        } else {
            draw::text(*lcd,
                status_area,
                fail_string.c_str(),
                font,
                color<typename Destination::pixel_type>::red);     
            ESP_LOGE(TAG_WIFI, "Could not disconnect from AP.");
            vTaskDelay(read_timout);                                            // Allow user to see status message
            return -1; // TODO proper error code
        }
    }


    // Apply staged config to wifiController
    Wifi.setStaConfig(*cfg);

    // Connect
    draw::text(*lcd,
            msg_area.offset(0, connected_string_size.height),
            connect_string.c_str(),
            font,
            color<typename Destination::pixel_type>::alice_blue);

    wifi_err_t status = Wifi.connect();
    switch(status) {
        case WIFI_OK:
            // Wait 10 seconds for connection or timeout
            for(int i = 0; i < 20; i++) {
                wifi_state = Wifi.GetState();
                if(wifi_state != espwifi::wifiController::state_e::DISCONNECTED || 
                    wifi_state != espwifi::wifiController::state_e::READY_TO_CONNECT) {       
                    vTaskDelay(pdMS_TO_TICKS(500));
                } else {
                    break;
                }
            }
            if(wifi_state == espwifi::wifiController::state_e::CONNECTED) {
                draw::text(*lcd,
                    status_area.offset(0, connected_string_size.height),
                    ok_string.c_str(),
                    font,
                    color<typename Destination::pixel_type>::green);
            } else if(wifi_state == espwifi::wifiController::state_e::DISCONNECTED) {
                draw::text(*lcd,
                    status_area.offset(0, connected_string_size.height),
                    fail_string.c_str(),
                    font,
                    color<typename Destination::pixel_type>::red);
                    vTaskDelay(read_timout);                                            // Allow user to see status message
                    return -1;
            } else {
                draw::text(*lcd,
                    status_area.offset(0, connected_string_size.height),
                    timeout_string.c_str(),
                    font,
                    color<typename Destination::pixel_type>::orange_red);
                    vTaskDelay(read_timout);                                            // Allow user to see status message
                    return -1;
            }
            break;
        case WIFI_ALRDY_CONNECTED:
            ESP_LOGD(TAG_MOD_SETTINGS,"FAILED (Already connected. Disconnect first.)");
            draw::text(*lcd,
                status_area.offset(0, connected_string_size.height),
                fail_string.c_str(),
                font,
                color<typename Destination::pixel_type>::red);
            vTaskDelay(read_timout);                                            // Allow user to see status message
            return -1;
            break;
        case WIFI_NOT_INIT:
            ESP_LOGD(TAG_MOD_SETTINGS, "FAILED (WiFi not properly initialised.)");
            draw::text(*lcd,
                status_area.offset(0, connected_string_size.height),
                fail_string.c_str(),
                font,
                color<typename Destination::pixel_type>::red);
            vTaskDelay(read_timout);                                            // Allow user to see status message
            return -1;
            break;
        default:
            draw::text(*lcd,
                status_area.offset(0, connected_string_size.height),
                fail_string.c_str(),
                font,
                color<typename Destination::pixel_type>::red);
            vTaskDelay(read_timout);                                            // Allow user to see status message
            return -1;
    }   

    vTaskDelay(read_timout);                                                    // Allow user to see status message
    return 0;

}


template<typename Destination>
int selectWifi(void *param) {

#ifdef CONFIG_DEBUG_STACK
    UBaseType_t uxHighWaterMark;

    /* Inspect our own high water mark on entering the task. */
    uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
    ESP_LOGD(CONFIG_TAG_STACK, "selectWifi(): High watermark for stack at start "
        "is: %d", uxHighWaterMark);
#endif

#ifdef CONFIG_DEBUG_HEAP
    multi_heap_info_t heap_info; 
    heap_caps_get_info(&heap_info, MALLOC_CAP_DEFAULT);
    ESP_LOGD(CONFIG_TAG_HEAP,   "demoMode(): Heap state at start: \n"
                                "            Free blocks:           %d\n"
                                "            Allocated blocks:      %d\n"
                                "            Total blocks:          %d\n"
                                "            Largest free block:    %d\n"
                                "            Total free bystes:     %d\n"
                                "            Total allocated bytes: %d\n"
                                "            Minimum free bytes:    %d\n"
                                , heap_info.free_blocks 
                                , heap_info.allocated_blocks
                                , heap_info.total_blocks
                                , heap_info.largest_free_block
                                , heap_info.total_free_bytes
                                , heap_info.total_allocated_bytes
                                , heap_info.minimum_free_bytes);
#endif // CONFIG_DEBUG_HEAP

    
#if !defined(CH405LABS_CONTROLLER_SUPPORT) || !defined(CONFIG_DISPLAY_SUPPORT)
    // If input or display not supported abort.
    ESP_LOGE(TAG_MOD_SETTINGS, "Input or display not supported. Aborting...");
    return -1;
#endif
    
    // Load wifi configuration
    espwifi::wifiController Wifi;
    static std::vector<espwifi::wifiStaConfig> loaded_configs; 

    wifi_err_t err = Wifi.loadConfigs(loaded_configs);
    if(err != WIFI_OK) {
        // TODO show error on screen
        ESP_LOGE(TAG_MOD_SETTINGS, "Failed loading configurations (%d)", err);
        return -1;
    } else {
        ESP_LOGD(TAG_MOD_SETTINGS,"Successfully loaded configurations.");
    }
    
    controllerDriver controller;
    controller_err_t controller_err = controller.config();

    if(controller_err != CONTROLLER_OK && controller_err != CONTROLLER_ALREADY_CONFIGURED) {
        ESP_LOGE(TAG_CONTROLLER, "Controller not functioning. (%d)", controller_err);
        return -1;
    }


    Destination *lcd = (Destination *)param;
    using lcd_color = color<typename Destination::pixel_type>;

    // Clear the LCD   
    lcd->clear(lcd->bounds());

    gfx::rect16 dst_bounds = rect16(lcd->bounds());
    static const size16 bmp_size(dst_bounds.width(), dst_bounds.height()); 

    // Try to allocate memory for buffering display output
    bool buffering = false;
    using bmp_type = bitmap<rgb_pixel<16>>;
    uint8_t *bmp_buf = NULL;
    bmp_type *bmp = NULL;
    
    bmp_buf = (uint8_t *)malloc(bmp_type::sizeof_buffer(bmp_size)*sizeof(uint8_t));
    if(bmp_buf != NULL) {
        bmp = (bmp_type *)malloc(sizeof(bmp_type));
        if(bmp != NULL) {
            bmp = new (bmp) bmp_type(bmp_size, bmp_buf);
            bmp->clear(bmp->bounds());
            buffering = true;
        } else {
            free(bmp_buf);
            bmp_buf = NULL;
        }
    } 

    if(buffering) {

        MenuController<bmp_type, bmp_type::pixel_type> mc;

        // Display stored wifi networks
        void *args[2] = {(void*)lcd, NULL};                                                          // Needs to be active while menu exists
        std::vector<espwifi::wifiStaConfig>::iterator cfg_iter;
        for(cfg_iter = loaded_configs.begin(); cfg_iter < loaded_configs.end(); cfg_iter++) {
            // TODO add menu item for each network config
            char ssid[32];
            cfg_iter->getSsid(ssid, sizeof(ssid));
            args[1] = (void*)&*cfg_iter;
            mc.cursor->addEntry(mc.createActionItem(ssid, connectWifi<Destination>, args, sizeof(args)));
        }

        mc.cursor->setToRoot();
        mc.cursor->first();
        mc.cursor->selectEntry();

        srect16 bounds = srect16(bmp->bounds());
        mc.cursor->drawMenu(*bmp, bounds, lcd_color::alice_blue, lcd_color::dark_goldenrod);
        draw::bitmap(*lcd, (srect16)lcd->bounds(), *bmp, bmp->bounds());

        while(true) {
            vTaskDelay(pdMS_TO_TICKS(250));                                     // Allow time to release button
            controller.capture();
            
            if(controller.getButtonState(BUTTON_DOWN)) {
                mc.cursor->deselectEntry();
                mc.cursor->next();
                mc.cursor->selectEntry();
                bounds = srect16(bmp->bounds());
                mc.cursor->drawMenu(*bmp, bounds, lcd_color::alice_blue, 
                    lcd_color::dark_goldenrod);
            } else if(controller.getButtonState(BUTTON_UP)) {
                mc.cursor->deselectEntry();
                mc.cursor->previous();
                mc.cursor->selectEntry();
                bounds = srect16(bmp->bounds());
                mc.cursor->drawMenu(*bmp, bounds, lcd_color::alice_blue, 
                    lcd_color::dark_goldenrod);
            } 

            if(controller.getButtonState(BUTTON_A) 
                || controller.getButtonState(BUTTON_B)) {

                const Entry<bmp_type, bmp_type::pixel_type> *e = 
                    mc.cursor->getEntry();
                if(e != NULL && typeid(*e) == typeid(ActionItem<bmp_type, 
                    bmp_type::pixel_type>)) {

                    int return_code = ((ActionItem<bmp_type, bmp_type::pixel_type> *)e)->execute();
                    if(return_code < 0) {
                    ESP_LOGE(TAG_MOD_SETTINGS, "Could not execute menut item. Code %d", return_code);
                    } else if (return_code > 0) {
                        ESP_LOGW(TAG_MOD_SETTINGS,"Menu execution returned with code %d.", return_code);
                    };
                    // Need to draw menu again, as execution could have used screen
                    bounds = srect16(bmp->bounds());
                    mc.cursor->drawMenu(*bmp, bounds, lcd_color::alice_blue, lcd_color::dark_goldenrod);
                } else if (e != NULL && typeid(*e) == typeid(Submenu<bmp_type, bmp_type::pixel_type>)) {
                    mc.cursor->enter();
                    if(mc.cursor->first() == GFXMENU_OK) {
                        mc.cursor->selectEntry();
                    }
                    bounds = srect16(bmp->bounds());
                    mc.cursor->drawMenu(*bmp, bounds, lcd_color::alice_blue, lcd_color::dark_goldenrod);
                }
            }

            if(controller.getButtonState(BUTTON_X) || controller.getButtonState(BUTTON_Y)) { 
                // We do not have submenus here, so just break the loop. TODO check in future
                break;
            }
            draw::bitmap(*lcd, (srect16)lcd->bounds(), *bmp, bmp->bounds());
        }

        //delete bmp;
        if(bmp != NULL) {
            bmp->~bmp_type();
            free(bmp);
        }
        
        if(bmp_buf != NULL) {
            free(bmp_buf);
        }

    } else {
        ESP_LOGE(TAG_MOD_SETTINGS, "Unbuffered display not implemented.");
    }
    
    return 0;
}
#endif // BCD_MODULE_SETTINGS_HPP